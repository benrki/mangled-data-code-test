const _ = require('lodash/fp');

const DIVISION_VALUE = 177;

const parseToInt = _.map(_.parseInt(10));
const getDigitMap = _.flow(_.toString, _.countBy(_.identity));

const containsAnagrams = arr => {
  const digitMaps = [];

  // TODO: make pure
  return _.any(n => {
    const digitMap = getDigitMap(n);
    if (_.any(_.isEqual(digitMap))(digitMaps)) {
      return true;
    } else {
      digitMaps.push(digitMap);
      return false;
    }
  })(arr);
};

const elementsCanDivideTo = num => arr => _.any(x => _.any(_.flow(_.divide(x), _.isEqual(num)))(arr))(arr);
const passesCriteria = _.overEvery([
  _.negate(containsAnagrams),
  _.negate(elementsCanDivideTo(DIVISION_VALUE))
]);
const getMinMaxDifference = (arr) => _.subtract(_.max(arr))(_.min(arr))

const getChecksum =_.flow(
  _.map(parseToInt),
  _.filter(passesCriteria),
  _.map(getMinMaxDifference),
  _.sum
);

module.exports = {
  elementsCanDivideTo,
  containsAnagrams,
  passesCriteria,
  getMinMaxDifference,
  getChecksum
};

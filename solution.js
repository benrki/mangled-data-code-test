'use strict';

const axios = require('axios');
const bluebird = require('bluebird');
const redis = require('redis');

const {
  containsAnagrams,
  elementsCanDivideTo,
  passesCriteria,
  getMinMaxDifference,
  getChecksum
} = require('./processing');

// Set up promises with redis client
bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);


const client = redis.createClient('redis://redis:6379');
const getKeys = client.keysAsync("*");


function getValueFromKey(key) {
  return client.typeAsync(key)
    .then((type) => {
      switch (type) {
        case 'set':
          return client.smembersAsync(key);
          break;
        case 'list':
          return client.lrangeAsync(key, 0, -1);
          break;
        default:
          return Promise.reject("Type not set or list");
          break;
      }
    });
}

function getValues(keys) {
  return Promise.all(keys.map(getValueFromKey))
}

function sendChecksum(checksum) {
  return axios.get(`http://answer:3000/${checksum}`)
}


getKeys
  .then(getValues)
  .then(getChecksum)
  .then(sendChecksum)
  .then(() => {
    console.info("Success");
    process.exit(0)
  }, (err) => {
    console.error("Error occurred", err);
    process.exit(1);
  });


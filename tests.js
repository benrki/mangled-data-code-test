const assert = require('assert');

const {
  containsAnagrams,
  elementsCanDivideTo,
  passesCriteria,
  getMinMaxDifference
} = require('./processing');


assert.ok(elementsCanDivideTo(7)([35, 5]));
assert.ok(!elementsCanDivideTo(177)([123123, 112313]));
assert.ok(containsAnagrams([1707, 7710]));
assert.ok(!containsAnagrams([177, 10]));
assert.ok(containsAnagrams([11283901832901, 83901832901112]));
assert.equal(300, getMinMaxDifference([1, 2, 3, 301]));
assert.equal(4, getMinMaxDifference([1, 2, 3, 4, 5]));
assert.ok(!passesCriteria([100, 150, 215, 80, 152]));
assert.ok(!passesCriteria([500, 354, 50, 2, 99]));
assert.ok(passesCriteria([1, 2, 3, 4, 5]));
assert.ok(passesCriteria([3001, 4, 1, 9, 500]));
assert.ok(!containsAnagrams([3001, 4, 1, 9, 500]));
assert.ok(!elementsCanDivideTo(177)[3001, 4, 1, 9, 500]);
assert.equal(3000, getMinMaxDifference([3001, 4, 1, 9, 500]));


console.info("All tests passed");

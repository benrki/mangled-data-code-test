FROM node:8-alpine

WORKDIR /app

COPY package.json yarn.lock ./
RUN yarn

COPY solution.js processing.js ./

CMD [ "npm", "start" ]
